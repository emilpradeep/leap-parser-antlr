package com.la.leapparser;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Stack;

public class MainListen {

    private static class Listener extends LeapParserBaseListener{

        private Stack<String> stack = new Stack<String>();

        public String getResult() {
            return stack.peek();
        }


        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void enterRelationship(LeapParserParser.RelationshipContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void exitRelationship(LeapParserParser.RelationshipContext ctx) {
            String RHS = stack.pop();
            String LHS = stack.pop();
            String operator = ctx.op.getText();
            String result = "Domain  \n" + LHS + "Range  \n" + RHS + "operator : " + operator;
            stack.push("\n" + result);
        }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void enterClassExpression(LeapParserParser.ClassExpressionContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void exitClassExpression(LeapParserParser.ClassExpressionContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void enterFieldExpression(LeapParserParser.FieldExpressionContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void exitFieldExpression(LeapParserParser.FieldExpressionContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void enterField_relation(LeapParserParser.Field_relationContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void exitField_relation(LeapParserParser.Field_relationContext ctx) {
            String fieldRelation = "Datasource : " + ctx.WORD(0).getText() + "\n" +
                    "Item : " + ctx.WORD(1).getText() + "\n" +
                    "Field : " + ctx.WORD(2).getText() + "\n";
            stack.push(fieldRelation);
        }

        @Override public void exitPredicateExpression(LeapParserParser.PredicateExpressionContext ctx) { }

        @Override public void exitPredicate_relation(LeapParserParser.Predicate_relationContext ctx) {

            String predicateRelation = "Class : " + ctx.CLASS(0).getText() + "\n" +
                    "Predicate : " + ctx.CLASS(1).getText() + "\n";
            stack.push(predicateRelation);
        }



        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void enterClass_relation(LeapParserParser.Class_relationContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void exitClass_relation(LeapParserParser.Class_relationContext ctx) {
            String classRelation = "Class : " + ctx.CLASS().getText() + "\n";
            stack.push(classRelation);
        }

        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void enterEveryRule(ParserRuleContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void exitEveryRule(ParserRuleContext ctx) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void visitTerminal(TerminalNode node) { }
        /**
         * {@inheritDoc}
         *
         * <p>The default implementation does nothing.</p>
         */
        @Override public void visitErrorNode(ErrorNode node) { }


    }

    public static void main(String[] args) throws Exception {
        String inputFile = null;
        if ( args.length>0 ) inputFile = args[0];
        InputStream is = System.in;
        if ( inputFile!=null ) is = new FileInputStream(inputFile);
        ANTLRInputStream input = new ANTLRInputStream(is);
        LeapParserLexer lexer = new LeapParserLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        LeapParserParser parser = new LeapParserParser(tokens);
        ParseTree tree = parser.expr();

        ParseTreeWalker walker = new ParseTreeWalker();
        MainListen.Listener listener = new MainListen.Listener();
        walker.walk(listener, tree);
        System.out.println(listener.getResult());

    }
}
