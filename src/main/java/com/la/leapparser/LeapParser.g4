grammar LeapParser;


expr : expr op=('=' | '>' | '<' | '>=' | '<=' | '!=') expr            #Relationship
     | field_relation              #FieldExpression
     | class_relation              #ClassExpression
     | predicate_relation          #PredicateExpression
     ;


field_relation : WORD DOT WORD DOT WORD ;
class_relation : CLASS ;
predicate_relation : CLASS DOT CLASS ;

fragment LOWERCASE : [a-z] ;
fragment UPPERCASE : [A-Z] ;



WORD : (LOWERCASE | UPPERCASE | '_')+ ;
CLASS : (LOWERCASE | UPPERCASE | '_' | ':')+ ;
EQUALS : '=' ;
DOT : '.' ;
COLON : ':' ;




WS : [ \t\r\n]+ -> skip ;