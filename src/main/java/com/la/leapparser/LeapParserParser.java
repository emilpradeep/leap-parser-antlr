package com.la.leapparser;// Generated from LeapParser.g4 by ANTLR 4.7.2

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeListener;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class LeapParserParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7.2", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, WORD=6, CLASS=7, EQUALS=8, DOT=9, 
		COLON=10, WS=11;
	public static final int
		RULE_expr = 0, RULE_field_relation = 1, RULE_class_relation = 2, RULE_predicate_relation = 3;
	private static String[] makeRuleNames() {
		return new String[] {
			"expr", "field_relation", "class_relation", "predicate_relation"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'>'", "'<'", "'>='", "'<='", "'!='", null, null, "'='", "'.'", 
			"':'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, null, null, null, null, null, "WORD", "CLASS", "EQUALS", "DOT", 
			"COLON", "WS"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "LeapParser.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public LeapParserParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ExprContext extends ParserRuleContext {
		public ExprContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class RelationshipContext extends ExprContext {
		public Token op;
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode EQUALS() { return getToken(LeapParserParser.EQUALS, 0); }
		public RelationshipContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).enterRelationship(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).exitRelationship(this);
		}
	}
	public static class ClassExpressionContext extends ExprContext {
		public Class_relationContext class_relation() {
			return getRuleContext(Class_relationContext.class,0);
		}
		public ClassExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).enterClassExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).exitClassExpression(this);
		}
	}
	public static class PredicateExpressionContext extends ExprContext {
		public Predicate_relationContext predicate_relation() {
			return getRuleContext(Predicate_relationContext.class,0);
		}
		public PredicateExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).enterPredicateExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).exitPredicateExpression(this);
		}
	}
	public static class FieldExpressionContext extends ExprContext {
		public Field_relationContext field_relation() {
			return getRuleContext(Field_relationContext.class,0);
		}
		public FieldExpressionContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).enterFieldExpression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).exitFieldExpression(this);
		}
	}

	public final ExprContext expr() throws RecognitionException {
		return expr(0);
	}

	private ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState);
		ExprContext _prevctx = _localctx;
		int _startState = 0;
		enterRecursionRule(_localctx, 0, RULE_expr, _p);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(12);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				{
				_localctx = new FieldExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(9);
				field_relation();
				}
				break;
			case 2:
				{
				_localctx = new ClassExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(10);
				class_relation();
				}
				break;
			case 3:
				{
				_localctx = new PredicateExpressionContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(11);
				predicate_relation();
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(19);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!= ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					{
					_localctx = new RelationshipContext(new ExprContext(_parentctx, _parentState));
					pushNewRecursionContext(_localctx, _startState, RULE_expr);
					setState(14);
					if (!(precpred(_ctx, 4))) throw new FailedPredicateException(this, "precpred(_ctx, 4)");
					setState(15);
					((RelationshipContext)_localctx).op = _input.LT(1);
					_la = _input.LA(1);
					if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__0) | (1L << T__1) | (1L << T__2) | (1L << T__3) | (1L << T__4) | (1L << EQUALS))) != 0)) ) {
						((RelationshipContext)_localctx).op = (Token)_errHandler.recoverInline(this);
					}
					else {
						if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
						_errHandler.reportMatch(this);
						consume();
					}
					setState(16);
					expr(5);
					}
					} 
				}
				setState(21);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class Field_relationContext extends ParserRuleContext {
		public List<TerminalNode> WORD() { return getTokens(LeapParserParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(LeapParserParser.WORD, i);
		}
		public List<TerminalNode> DOT() { return getTokens(LeapParserParser.DOT); }
		public TerminalNode DOT(int i) {
			return getToken(LeapParserParser.DOT, i);
		}
		public Field_relationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_field_relation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).enterField_relation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).exitField_relation(this);
		}
	}

	public final Field_relationContext field_relation() throws RecognitionException {
		Field_relationContext _localctx = new Field_relationContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_field_relation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(22);
			match(WORD);
			setState(23);
			match(DOT);
			setState(24);
			match(WORD);
			setState(25);
			match(DOT);
			setState(26);
			match(WORD);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Class_relationContext extends ParserRuleContext {
		public TerminalNode CLASS() { return getToken(LeapParserParser.CLASS, 0); }
		public Class_relationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_class_relation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).enterClass_relation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).exitClass_relation(this);
		}
	}

	public final Class_relationContext class_relation() throws RecognitionException {
		Class_relationContext _localctx = new Class_relationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_class_relation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(28);
			match(CLASS);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Predicate_relationContext extends ParserRuleContext {
		public List<TerminalNode> CLASS() { return getTokens(LeapParserParser.CLASS); }
		public TerminalNode CLASS(int i) {
			return getToken(LeapParserParser.CLASS, i);
		}
		public TerminalNode DOT() { return getToken(LeapParserParser.DOT, 0); }
		public Predicate_relationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_predicate_relation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).enterPredicate_relation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof LeapParserListener ) ((LeapParserListener)listener).exitPredicate_relation(this);
		}
	}

	public final Predicate_relationContext predicate_relation() throws RecognitionException {
		Predicate_relationContext _localctx = new Predicate_relationContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_predicate_relation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(30);
			match(CLASS);
			setState(31);
			match(DOT);
			setState(32);
			match(CLASS);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 0:
			return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0:
			return precpred(_ctx, 4);
		}
		return true;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\r%\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\3\2\3\2\3\2\3\2\5\2\17\n\2\3\2\3\2\3\2\7\2\24\n\2\f"+
		"\2\16\2\27\13\2\3\3\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\5\3\5\3\5\3\5\3\5\2"+
		"\3\2\6\2\4\6\b\2\3\4\2\3\7\n\n\2#\2\16\3\2\2\2\4\30\3\2\2\2\6\36\3\2\2"+
		"\2\b \3\2\2\2\n\13\b\2\1\2\13\17\5\4\3\2\f\17\5\6\4\2\r\17\5\b\5\2\16"+
		"\n\3\2\2\2\16\f\3\2\2\2\16\r\3\2\2\2\17\25\3\2\2\2\20\21\f\6\2\2\21\22"+
		"\t\2\2\2\22\24\5\2\2\7\23\20\3\2\2\2\24\27\3\2\2\2\25\23\3\2\2\2\25\26"+
		"\3\2\2\2\26\3\3\2\2\2\27\25\3\2\2\2\30\31\7\b\2\2\31\32\7\13\2\2\32\33"+
		"\7\b\2\2\33\34\7\13\2\2\34\35\7\b\2\2\35\5\3\2\2\2\36\37\7\t\2\2\37\7"+
		"\3\2\2\2 !\7\t\2\2!\"\7\13\2\2\"#\7\t\2\2#\t\3\2\2\2\4\16\25";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}