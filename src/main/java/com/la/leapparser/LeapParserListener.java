package com.la.leapparser;// Generated from LeapParser.g4 by ANTLR 4.7.2
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link LeapParserParser}.
 */
public interface LeapParserListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by the {@code Relationship}
	 * labeled alternative in {@link LeapParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterRelationship(LeapParserParser.RelationshipContext ctx);
	/**
	 * Exit a parse tree produced by the {@code Relationship}
	 * labeled alternative in {@link LeapParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitRelationship(LeapParserParser.RelationshipContext ctx);
	/**
	 * Enter a parse tree produced by the {@code ClassExpression}
	 * labeled alternative in {@link LeapParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterClassExpression(LeapParserParser.ClassExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code ClassExpression}
	 * labeled alternative in {@link LeapParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitClassExpression(LeapParserParser.ClassExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code PredicateExpression}
	 * labeled alternative in {@link LeapParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterPredicateExpression(LeapParserParser.PredicateExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code PredicateExpression}
	 * labeled alternative in {@link LeapParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitPredicateExpression(LeapParserParser.PredicateExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code FieldExpression}
	 * labeled alternative in {@link LeapParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void enterFieldExpression(LeapParserParser.FieldExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code FieldExpression}
	 * labeled alternative in {@link LeapParserParser#expr}.
	 * @param ctx the parse tree
	 */
	void exitFieldExpression(LeapParserParser.FieldExpressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeapParserParser#field_relation}.
	 * @param ctx the parse tree
	 */
	void enterField_relation(LeapParserParser.Field_relationContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeapParserParser#field_relation}.
	 * @param ctx the parse tree
	 */
	void exitField_relation(LeapParserParser.Field_relationContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeapParserParser#class_relation}.
	 * @param ctx the parse tree
	 */
	void enterClass_relation(LeapParserParser.Class_relationContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeapParserParser#class_relation}.
	 * @param ctx the parse tree
	 */
	void exitClass_relation(LeapParserParser.Class_relationContext ctx);
	/**
	 * Enter a parse tree produced by {@link LeapParserParser#predicate_relation}.
	 * @param ctx the parse tree
	 */
	void enterPredicate_relation(LeapParserParser.Predicate_relationContext ctx);
	/**
	 * Exit a parse tree produced by {@link LeapParserParser#predicate_relation}.
	 * @param ctx the parse tree
	 */
	void exitPredicate_relation(LeapParserParser.Predicate_relationContext ctx);
}